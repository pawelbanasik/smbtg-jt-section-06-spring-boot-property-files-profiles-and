package com.pawelbanasik.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.pawelbanasik.examplebeans.FakeDataSource;
import com.pawelbanasik.examplebeans.FakeJmsBroker;

// jak wrzuce wszystko do application.properties to wszystko to zakomentowane staje sie zbedne!!!
@Configuration
//@PropertySource({ "classpath:datasource.properties", "classpath:jms.properties" })
//@PropertySources({ @PropertySource("classpath:datasource.properties"), @PropertySource("classpath:jms.properties") })
public class SpringConfig {

	// inny sposob tworzenia beanow - ja wole w klasach osobnych
	@Value("${guru.username}")
	private String username;

	@Value("${guru.password}")
	private String password;

	@Value("${guru.url}")
	private String url;

	@Value("${guru.jms.username}")
	private String jmsUsername;

	@Value("${guru.jms.password}")
	private String jmsPassword;

	@Value("${guru.jms.url}")
	private String jmsUrl;

	@Bean
	public FakeDataSource fakeDataSource() {
		FakeDataSource fakeDataSource = new FakeDataSource();
		fakeDataSource.setUsername(username);
		fakeDataSource.setPassword(password);
		fakeDataSource.setUrl(url);
		return fakeDataSource;
	}

	@Bean
	public FakeJmsBroker fakeJmsBroker() {
		FakeJmsBroker fakeJmsBroker = new FakeJmsBroker();
		fakeJmsBroker.setUsername(jmsUsername);
		fakeJmsBroker.setPassword(jmsPassword);
		fakeJmsBroker.setUrl(jmsUrl);
		return fakeJmsBroker;
	}

//	@Bean
//	public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
//		PropertySourcesPlaceholderConfigurer placeholderConfigurer = new PropertySourcesPlaceholderConfigurer();
//		return new PropertySourcesPlaceholderConfigurer();
//	}

}
