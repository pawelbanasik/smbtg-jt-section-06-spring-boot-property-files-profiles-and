package com.pawelbanasik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.pawelbanasik.examplebeans.FakeDataSource;
import com.pawelbanasik.examplebeans.FakeJmsBroker;

@SpringBootApplication
public class SmbtgJtSection5SpringBootPropertyFilesApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(SmbtgJtSection5SpringBootPropertyFilesApplication.class,
				args);
		FakeDataSource fakeDataSource = context.getBean(FakeDataSource.class);
		System.out.println(fakeDataSource.getUsername());

		FakeJmsBroker fakeJmsBroker = context.getBean(FakeJmsBroker.class);
		System.out.println(fakeJmsBroker.getUsername());
	}
}
